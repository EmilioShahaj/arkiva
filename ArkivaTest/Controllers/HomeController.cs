﻿using ArkivaTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Entity;
using System.Web.Mvc;
using System.Net;
using ArkivaTest.ViewModels;
using System.IO;
using Microsoft.AspNet.Identity;
using System.IO.Compression;

namespace ArkivaTest.Controllers
{
    public class HomeController : Controller
    {

        private ApplicationDbContext _context;

        public HomeController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Documents/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Document document = _context.Documents.Find(id);
            if (document == null)
            {
                return HttpNotFound();
            }
            return View(document);
        }

        // GET: Documents/Create
        public ActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }

            var subjects = _context.Subjects.ToList();
            var inspections = _context.Inspections.ToList();
            var documentTypes = _context.DocumentTypes.ToList();
            var documents = _context.Documents.Include(d => d.ApplicationUser).Include(d => d.DocumentType).Include(d => d.Tags).ToList();
            var tags = _context.Tags.Include(t => t.Documents).ToList();
            var viewModel = new ArchiveViewModel
            {

                Subjects = subjects,
                Inspections = inspections,
                DocumentTypes = documentTypes,
                Documents = documents,
                Tags = tags
            };

            return View("Index", viewModel);
        }


        // POST: Documents/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Document document, Tag tag, HttpPostedFileBase[] uploadedFiles)
        {
            document.ApplicationUserId = User.Identity.GetUserId();
            var subjects = _context.Subjects.ToList();
            var tags = _context.Tags.Include(t => t.Documents).ToList();
            var inspections = _context.Inspections.ToList();
            var documents = _context.Documents.Include(d => d.ApplicationUser).Include(d => d.DocumentType).Include(d => d.Tags).ToList();
            var documentTypes = _context.DocumentTypes.ToList();
            var viewModel = new ArchiveViewModel
            {
                UploadedFiles = uploadedFiles,
                Tags = tags,
                Documents = documents,
                Subjects = subjects,
                Inspections = inspections,
                DocumentTypes = documentTypes
            };

            if (ModelState.IsValid)
            {

                string line = tag.TagName;
                List<string> stringList = line.Split(',').ToList();

                foreach (var file in uploadedFiles)
                {

                    byte[] bytes;
                    using (BinaryReader br = new BinaryReader(file.InputStream))
                    {
                        bytes = br.ReadBytes(file.ContentLength);
                    }

                    var documentToAdd = new Document
                    {
                        DocumentName = file.FileName,
                        DocumentData = bytes,
                        CreatedOn = DateTime.Now,
                        Location = document.Location,
                        BoxNumber = document.BoxNumber,
                        Shelf = document.Shelf,
                        DocumentTypeId = document.DocumentTypeId,
                        ApplicationUserId = document.ApplicationUserId,
                        Tags = document.Tags,

                    };

                    foreach (var str in stringList)
                    {
                        tag.TagName = str;

                        var tagInDb = _context.Tags.Where(t => t.TagName == str).FirstOrDefault();

                        if (tagInDb != null)
                        {
                            tagInDb.Documents.Add(documentToAdd);
                        }
                        else
                        {
                            var tagNotInDb = new Tag
                            {
                                TagName = str,
                            };

                            _context.Tags.Add(tagNotInDb);
                            tagNotInDb.Documents.Add(documentToAdd);
                            _context.SaveChanges();
                        }
                    }
                }

                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View("Index", viewModel);
        }

        // GET: Documents/Edit/5
        public ActionResult Edit(int id)
        {
            var document = _context.Documents.Find(id);

            var subjects = _context.Subjects.ToList();
            var tags = _context.Tags.Include(t => t.Documents).ToList();
            var inspections = _context.Inspections.ToList();
            var documents = _context.Documents.Include(d => d.ApplicationUser).Include(d => d.DocumentType).Include(d => d.Tags).ToList();
            var documentTypes = _context.DocumentTypes.ToList();

            var viewModel = new ArchiveViewModel
            {
                Document = document,
                Tags = tags,
                Documents = documents,
                Subjects = subjects,
                Inspections = inspections,
                DocumentTypes = documentTypes
            };
            return View("Index", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Document document, Tag tag)
        {
            var subjects = _context.Subjects.ToList();
            var tags = _context.Tags.Include(t => t.Documents).ToList();
            var inspections = _context.Inspections.ToList();
            var documents = _context.Documents.Include(d => d.ApplicationUser).Include(d => d.DocumentType).Include(d => d.Tags).ToList();
            var documentTypes = _context.DocumentTypes.ToList();
            var viewModel = new ArchiveViewModel
            {
                Tags = tags,
                Documents = documents,
                Subjects = subjects,
                Inspections = inspections,
                DocumentTypes = documentTypes
            };

            if (ModelState.IsValid)
            {
                var documentInDb = _context.Documents.Single(x => x.Id == document.Id);
                documentInDb.Tags = new List<Tag>();
                string line = tag.TagName;
                List<string> stringListToEdit = line.Split(',').ToList();
                foreach (var str in stringListToEdit)
                {

                    tag.TagName = str;
                    var tagInDb = _context.Tags.Where(t => t.TagName == str).FirstOrDefault();

                    if (tagInDb != null)
                    {
                        documentInDb.Tags.Add(tagInDb);
                    }
                    else
                    {
                        var tagNotInDb = new Tag
                        {
                            TagName = str,
                        };
                        _context.Tags.Add(tagNotInDb);
                        documentInDb.Tags.Add(tagNotInDb);
                        _context.SaveChanges();
                    }
                }


                documentInDb.Location = document.Location;
                documentInDb.BoxNumber = document.BoxNumber;
                documentInDb.Shelf = document.Shelf;
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View("Index", viewModel);
        }

        // GET: Documents/Delete/5
        public ActionResult Delete(int id)
        {
            var document = _context.Documents.Find(id);
            _context.Documents.Remove(document);
            _context.SaveChanges();

            return View("Index");
        }




        public Boolean DocumentTypeHasDocuments(int documentTypeId)
        {
            var documentTypeHasDocuments = _context.Documents.Where(d => d.DocumentTypeId == documentTypeId).FirstOrDefault();

            if (documentTypeHasDocuments != null)
                return true;
            else
                return false;
        }



        public ActionResult Search(ArchiveViewModel searchModel)
        {
            var documentsInDb = _context.Documents.ToList();
            var documentTypesInDb = _context.DocumentTypes.ToList();
            var inspectionsInDb = _context.Inspections.ToList();
            var subjectsInDb = _context.Subjects.ToList();

            var viewModel = new ArchiveViewModel
            {
                DocumentNameSearch = searchModel.DocumentNameSearch,
                DocumentTypeNameSearch = searchModel.DocumentTypeNameSearch,
                InspectionNumberSearch = searchModel.InspectionNumberSearch,
                SubjectNameSearch = searchModel.SubjectNameSearch,
                LocationSearch = searchModel.LocationSearch,
                BoxNumberSearch = searchModel.BoxNumberSearch,
                ShelfSearch = searchModel.ShelfSearch,
                FromDateSearch = searchModel.FromDateSearch,
                ToDateSearch = searchModel.ToDateSearch,
                TagNameSearch = searchModel.TagNameSearch,

                Documents = GetDocuments(searchModel),
                DocumentTypes = GetDocumentTypes(searchModel),
                Inspections = GetInspections(searchModel),
                Subjects = GetSubjects(searchModel),
                DocumentsInDb = documentsInDb,
                DocumentTypesInDb = documentTypesInDb,
                InspectionsInDb = inspectionsInDb,
                SubjectsInDb = subjectsInDb
            };

            return PartialView("_SearchResults", viewModel);

        }


        public IQueryable<Document> GetDocuments(ArchiveViewModel searchModel)
        {
            var result = _context.Documents.AsQueryable();
            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.DocumentNameSearch))
                    result = result.Where(x => x.DocumentName == searchModel.DocumentNameSearch);
                if (!string.IsNullOrEmpty(searchModel.LocationSearch))
                    result = result.Where(x => x.Location == searchModel.LocationSearch);
                if (!string.IsNullOrEmpty(searchModel.BoxNumberSearch))
                    result = result.Where(x => x.BoxNumber == searchModel.BoxNumberSearch);
                if (!string.IsNullOrEmpty(searchModel.ShelfSearch))
                    result = result.Where(x => x.Shelf == searchModel.ShelfSearch);
                if (searchModel.FromDateSearch.HasValue)
                    result = result.Where(x => x.CreatedOn >= searchModel.FromDateSearch);
                if (searchModel.ToDateSearch.HasValue)
                    result = result.Where(x => x.CreatedOn <= searchModel.ToDateSearch);

                if (!string.IsNullOrEmpty(searchModel.TagNameSearch))
                {

                    string line = searchModel.TagNameSearch;
                    List<string> stringList = line.Split(',').ToList();
                    foreach (var tag in stringList)
                        result = result.Where(x => x.Tags.Select(t => t.TagName).Contains(tag));
                }

                if (!string.IsNullOrEmpty(searchModel.DocumentTypeNameSearch))
                    result = result.Where(x => x.DocumentType.TypeName == searchModel.DocumentTypeNameSearch);

                if (!string.IsNullOrEmpty(searchModel.InspectionNumberSearch))
                    result = result.Where(x => x.DocumentType.Inspection.InspectionNumber.Contains(searchModel.InspectionNumberSearch));

                if (!string.IsNullOrEmpty(searchModel.SubjectNameSearch))
                    result = result.Where(x => x.DocumentType.Inspection.Subject.SubjectName.Contains(searchModel.SubjectNameSearch));
            }
            return result;
        }

        public IQueryable<DocumentType> GetDocumentTypes(ArchiveViewModel searchModel)
        {
            var result = _context.DocumentTypes.AsQueryable();
            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.DocumentTypeNameSearch))
                    result = result.Where(x => x.TypeName == searchModel.DocumentTypeNameSearch);
                if (!string.IsNullOrEmpty(searchModel.InspectionNumberSearch))
                    result = result.Where(x => x.Inspection.InspectionNumber.Contains(searchModel.InspectionNumberSearch));
                if (!string.IsNullOrEmpty(searchModel.SubjectNameSearch))
                    result = result.Where(x => x.Inspection.Subject.SubjectName.Contains(searchModel.SubjectNameSearch));
            }
            return result;
        }

        public IQueryable<Inspection> GetInspections(ArchiveViewModel searchModel)
        {
            var result = _context.Inspections.AsQueryable();
            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.InspectionNumberSearch))
                    result = result.Where(x => x.InspectionNumber.Contains(searchModel.InspectionNumberSearch));
                if (!string.IsNullOrEmpty(searchModel.SubjectNameSearch))
                    result = result.Where(x => x.Subject.SubjectName.Contains(searchModel.SubjectNameSearch));
            }
            return result;
        }

        public IQueryable<Subject> GetSubjects(ArchiveViewModel searchModel)
        {
            var result = _context.Subjects.AsQueryable();
            if (searchModel != null)
            {
                if (!string.IsNullOrEmpty(searchModel.SubjectNameSearch))
                    result = result.Where(x => x.SubjectName.Contains(searchModel.SubjectNameSearch));
            }
            return result;
        }

        public string GetDocumentPath(Document document)
        {
            var path = "Subjektet/" + document.DocumentType.Inspection.Subject.SubjectName + "/" + document.DocumentType.Inspection.InspectionNumber + "/" + document.DocumentType.TypeName + "/" + document.DocumentName;
            return path;
        }

        public ActionResult Download(int id)
        {
            var documentToDownload = _context.Documents.Find(id);
            byte[] documentData = documentToDownload.DocumentData;

            return File(documentData, "application/octet-stream", documentToDownload.DocumentName);
        }

        public ActionResult DownloadZip(int id)
        {
            var documentTypeToZip = _context.DocumentTypes.Find(id);
            var documentsToDownload = _context.Documents.Where(x => x.DocumentTypeId == documentTypeToZip.Id).ToList();

            var compressedFileStream = new MemoryStream();
            using (var zipArchive = new ZipArchive(compressedFileStream, ZipArchiveMode.Create, false))
            {
                foreach (var caseAttachmentModel in documentsToDownload)
                {
                    //Create a zip entry for each attachment
                    var zipEntry = zipArchive.CreateEntry(caseAttachmentModel.DocumentName);

                    //Get the stream of the attachment
                    using (var originalFileStream = new MemoryStream(caseAttachmentModel.DocumentData))
                    using (var zipEntryStream = zipEntry.Open())
                    {
                        //Copy the attachment stream to the zip entry stream
                        originalFileStream.CopyTo(zipEntryStream);
                    }
                }
            }
            return new FileContentResult(compressedFileStream.ToArray(), "application/zip") { FileDownloadName = documentTypeToZip.TypeName + ".zip" };
        }


        public ActionResult FillEditForm(int id)
        {
            var documentToEdit = _context.Documents.Find(id);
            var result = new Document
            {
                Location = documentToEdit.Location,
                BoxNumber = documentToEdit.BoxNumber,
                Shelf = documentToEdit.Shelf,
            };
            List<string> stringList = documentToEdit.Tags.Select(x => x.TagName).ToList();
            string line = string.Join(",", stringList);
            var resultTag = new Tag
            {
                TagName = line
            };
            return Json(new { result, resultTag }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult PreviewDocument(int id)
        {
            var document = _context.Documents.Find(id);

            if (!document.DocumentName.EndsWith(".pdf"))
            {
                string result1="";
                return Json(result1, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = Convert.ToBase64String(document.DocumentData);
                var JsonResult = Json(result, JsonRequestBehavior.AllowGet);
                JsonResult.MaxJsonLength = int.MaxValue;
                return JsonResult;
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

    }



}
