﻿using ArkivaTest.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArkivaTest.ViewModels
{
    public class ArchiveViewModel
    {
        public Document Document { get; set; }

        public IEnumerable<Document> Documents { get; set; }

        public DocumentType DocumentType { get; set; }

        public IEnumerable<DocumentType> DocumentTypes { get; set; }

        public Subject Subject { get; set; }

        public IEnumerable<Subject> Subjects { get; set; }

        public Inspection Inspection { get; set; }

        public IEnumerable<Inspection> Inspections { get; set; }

        public Tag Tag { get; set; }
        
        public IEnumerable<Tag> Tags { get; set; }

        [Required(ErrorMessage = "Zgjidhni të paktën një dokument.")]
        public IEnumerable<HttpPostedFileBase> UploadedFiles { get; set; }

        public IEnumerable<Document> DocumentsInDb { get; set; }
        public IEnumerable<DocumentType> DocumentTypesInDb { get; set; }
        public IEnumerable<Inspection> InspectionsInDb { get; set; }
        public IEnumerable<Subject> SubjectsInDb { get; set; }


        public string SubjectNameSearch { get; set; }
        public string InspectionNumberSearch { get; set; }
        public string DocumentTypeNameSearch { get; set; }
        public string DocumentNameSearch { get; set; }
        public string TagNameSearch { get; set; }
        public string LocationSearch { get; set; }
        public string BoxNumberSearch { get; set; }
        public string ShelfSearch { get; set; }
        public DateTime? FromDateSearch { get; set; }
        public DateTime? ToDateSearch { get; set; }

    }
}