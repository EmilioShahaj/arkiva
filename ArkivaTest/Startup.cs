﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ArkivaTest.Startup))]
namespace ArkivaTest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
