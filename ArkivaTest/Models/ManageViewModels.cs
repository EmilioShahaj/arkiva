﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace ArkivaTest.Models
{
    public class IndexViewModel
    {
        public bool HasPassword { get; set; }
        public IList<UserLoginInfo> Logins { get; set; }
        public string PhoneNumber { get; set; }
        public bool TwoFactor { get; set; }
        public bool BrowserRemembered { get; set; }
    }

    public class ManageLoginsViewModel
    {
        public IList<UserLoginInfo> CurrentLogins { get; set; }
        public IList<AuthenticationDescription> OtherLogins { get; set; }
    }

    public class FactorViewModel
    {
        public string Purpose { get; set; }
    }

    public class SetPasswordViewModel
    {
        [Required(ErrorMessage = "Vendosni fjalëkalimin!")]
        [StringLength(100, ErrorMessage = "Fjalëkalimi duhet të ketë të paktën 6 karaktere.")]
        [DataType(DataType.Password, ErrorMessage = "Fjalëkalimi duhet të përmbajë të paktën një numër, një shkronjë të madhe dhe një simbol.")]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password, ErrorMessage = "Fjalëkalimi duhet të përmbajë të paktën një numër, një shkronjë të madhe dhe një simbol.")]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "Fjalëkalimet nuk përputhen.")]
        public string ConfirmPassword { get; set; }
    }

    public class ChangePasswordViewModel
    {
        [Required(ErrorMessage = "Vendosni fjalëkalimin.")]
        [DataType(DataType.Password, ErrorMessage = "Fjalëkalimi duhet të përmbajë të paktën një numër, një shkronjë të madhe dhe një simbol.")]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Vendosni fjalëkalimin.")]
        [StringLength(100, ErrorMessage = "Fjalëkalimi duhet të ketë të paktën 6 karaktere.")]
        [DataType(DataType.Password, ErrorMessage = "Fjalëkalimi duhet të përmbajë të paktën një numër, një shkronjë të madhe dhe një simbol.")]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password, ErrorMessage = "Fjalëkalimi duhet të përmbajë të paktën një numër, një shkronjë të madhe dhe një simbol.")]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "Fjalëkalimet nuk përputhen.")]
        public string ConfirmPassword { get; set; }
    }

    public class AddPhoneNumberViewModel
    {
        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string Number { get; set; }
    }

    public class VerifyPhoneNumberViewModel
    {
        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
    }

    public class ConfigureTwoFactorViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
    }
}