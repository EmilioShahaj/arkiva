﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations.Schema;

namespace ArkivaTest.Models
{
        public class Document
        {
            public Document()
            {
                this.Tags = new HashSet<Tag>();
            }
            public int Id { get; set; }

            public DateTime? CreatedOn { get; set; }

            public string DocumentName { get; set; }

            public string Location { get; set; }

            public string Shelf { get; set; }

            public string BoxNumber { get; set; }


            public DocumentType DocumentType { get; set; }

            [Required(ErrorMessage ="Zgjidhni llojin e dokumentit.")]
            [Display(Name ="Lloj dokumenti")]
            public int DocumentTypeId { get; set; }

            public virtual ApplicationUser ApplicationUser { get; set; }


            public string ApplicationUserId { get; set; }


            public byte[] DocumentData { get; set; }

            public virtual ICollection<Tag> Tags { get; set; }
       
        }
}