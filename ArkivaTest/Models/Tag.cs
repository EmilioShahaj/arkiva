﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArkivaTest.Models
{
    public class Tag
    {
        public Tag()
        {
            this.Documents = new HashSet<Document>();
        }
        public int Id { get; set; }

        [Required(ErrorMessage = "Vendosni të paktën një tag.")]
        public string TagName { get; set; }

        public virtual ICollection<Document> Documents { get; set; }

    }
}