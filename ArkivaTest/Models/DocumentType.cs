﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArkivaTest.Models
{
    public class DocumentType
    {
        public int Id { get; set; }
        public string TypeName { get; set; }

        public int InspectionId { get; set; }
        public Inspection Inspection { get; set; }

        public ICollection<Document> Documents { get; set; }
    }
}