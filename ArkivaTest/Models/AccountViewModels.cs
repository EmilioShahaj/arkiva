﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ArkivaTest.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required(ErrorMessage = "Vendosni Email!")]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required(ErrorMessage ="Vendosni email.")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Formati i email është i pasaktë.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Vendosni fjalëkalimin.")]
        [DataType(DataType.Password, ErrorMessage = "Fjalëkalimi duhet të përmbajë të paktën një numër, një shkronjë të madhe dhe një simbol.")]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Vendosni email.")]
        [EmailAddress(ErrorMessage = "Formati i email është i pasaktë.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Vendosni fjalëkalimin.")]
        [StringLength(100, ErrorMessage = "Fjalëkalimi duhet të ketë të paktën 6 karaktere.")]
        [DataType(DataType.Password, ErrorMessage = "Fjalëkalimi duhet të përmbajë të paktën një numër, një shkronjë të madhe dhe një simbol.")]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password, ErrorMessage = "Fjalëkalimi duhet të përmbajë të paktën një numër, një shkronjë të madhe dhe një simbol.")]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "Fjalëkalimet nuk përputhen.")]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required(ErrorMessage = "Vendosni email.")]
        [EmailAddress(ErrorMessage = "Formati i email është i pasaktë.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Vendosni fjalëkalimin.")]
        [StringLength(100, ErrorMessage = "Fjalëkalimi duhet të ketë të paktën 6 karaktere.")]
        [DataType(DataType.Password,ErrorMessage = "Fjalëkalimi duhet të përmbajë të paktën një numër, një shkronjë të madhe dhe një simbol.")]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password, ErrorMessage = "Fjalëkalimi duhet të përmbajë të paktën një numër, një shkronjë të madhe dhe një simbol.")]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "Fjalëkalimet nuk përputhen.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "Vendosni email.")]
        [EmailAddress(ErrorMessage = "Formati i email është i pasaktë.")]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
