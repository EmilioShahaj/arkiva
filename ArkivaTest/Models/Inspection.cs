﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArkivaTest.Models
{
    public class Inspection
    {
        public int Id { get; set; }


        public DateTime InspectionDate { get; set; }

        public string InspectionNumber { get; set; }

        public Subject Subject { get; set; }

        public int SubjectId { get; set; }

    }
 

}