namespace ArkivaTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class userIdNotrequired : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Documents", new[] { "ApplicationUserId" });
            AlterColumn("dbo.Documents", "ApplicationUserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Documents", "ApplicationUserId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Documents", new[] { "ApplicationUserId" });
            AlterColumn("dbo.Documents", "ApplicationUserId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Documents", "ApplicationUserId");
        }
    }
}
