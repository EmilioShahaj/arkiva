namespace ArkivaTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RequiredDocName : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Documents", "DocumentName", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Documents", "DocumentName", c => c.String(nullable: false));
        }
    }
}
