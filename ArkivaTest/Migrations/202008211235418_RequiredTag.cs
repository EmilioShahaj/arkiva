namespace ArkivaTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RequiredTag : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Tags", "TagName", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tags", "TagName", c => c.String());
        }
    }
}
