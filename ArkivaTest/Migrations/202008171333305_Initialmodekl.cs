namespace ArkivaTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initialmodekl : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Documents", "InspectionId", "dbo.Inspections");
            DropIndex("dbo.Documents", new[] { "InspectionId" });
            AddColumn("dbo.DocumentTypes", "InspectionId", c => c.Int(nullable: false));
            CreateIndex("dbo.DocumentTypes", "InspectionId");
            AddForeignKey("dbo.DocumentTypes", "InspectionId", "dbo.Inspections", "Id", cascadeDelete: true);
            DropColumn("dbo.Documents", "InspectionId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Documents", "InspectionId", c => c.Int(nullable: false));
            DropForeignKey("dbo.DocumentTypes", "InspectionId", "dbo.Inspections");
            DropIndex("dbo.DocumentTypes", new[] { "InspectionId" });
            DropColumn("dbo.DocumentTypes", "InspectionId");
            CreateIndex("dbo.Documents", "InspectionId");
            AddForeignKey("dbo.Documents", "InspectionId", "dbo.Inspections", "Id", cascadeDelete: true);
        }
    }
}
